import csv
import pathlib
import operator
import math
import prettytable
import logging
import numpy as np
import pandas as pd
import configparser
# unicode ascii transliterator module
import unidecode
## internal imports -- these happen after logging and config because otherwise they don't work :P
#import card_data
import synergy_data as SD



## Logging setup
logger = logging.getLogger(__name__)
logging.basicConfig(format="%(asctime)s | %(levelname)s |%(module)s | %(message)s", level=logging.INFO)
logger.info("Logging test")


## configparser stuff

cfg_file = "analyze.ini"
cfg = configparser.ConfigParser()
cfg.read(cfg_file)

# curve constants
curve_k = cfg['Constants'].getfloat('curve_k')
curve_center = cfg['Constants'].getfloat('curve_center')
curve_min = cfg['Constants'].getfloat('curve_min')

# tutor constants
tutor_k = cfg['Constants'].getfloat('tutor_k')
tutor_center = cfg['Constants'].getfloat('tutor_center')
tutor_offset = cfg['Constants'].getfloat('tutor_offset')

# file and folder vars
decks_folder = cfg['Files'].get('decks_folder')
reports_folder = cfg['Files'].get('reports_folder')
# magic card db file
card_dataset_file = cfg['Files'].get('card_dataset_file')
# synergy dataset file
synergy_dataset_file = cfg['Files'].get('synergy_dataset_file')

# data dictionary
tags_field = cfg['Data'].get('tags_field')
tutor_tag = cfg['Data'].get('tutor_tag')
land_type = cfg['Data'].get('land_type')
commander_max = cfg['Data'].getint('commander_max') ## max number of commanders we can detect



# Configure synergies

## Spin up the synergies and card data objects
synergy_data = SD.SynergyData(pathlib.Path(synergy_dataset_file))
card_data = SD.CardData(card_dataset_file)


## Storage for list of decks we will be analyzing
decks = []

class Deck:


    ## The deck is dependent on synergy data since it
    ## needs to be aware of synergies to report on them


    ## We create a class object with a list of cards and a list of
    def __init__(self, name: str, commanders: list, cards: list,
        curve_k:float, curve_center:float, curve_min:float, # curve stuff
        tutor_k:float, tutor_center:float, tutor_offset:float
        ):
        self.name = name
        self.commanders = sorted(commanders, reverse=True)
        self.cards = sorted(cards, reverse=True)
        ## create an internal var of a set of all the card/commander names
        self.cards_set = {card.lazy_name for card in cards} | {commander.lazy_name for commander in self.commanders}
        self.commanders_set = {commander.name for commander in self.commanders}
        # initial score is always 0!
        self.score = 0
        ### we should probably make it score itself at load, but dunno
        logger.debug("Before get_synergies")
        self.synergies = self.get_synergies()
        logger.debug("After get_synergies")
    
        ## ingesting constants!
        #curve stuff
        self.curve_k = curve_k # k that makes sure the formula is not negative
        self.curve_center = curve_center # assumed median curve approximate
        self.curve_min = curve_min # minimum multiplier value for curve
        #tutor stuff
        self.tutor_k = tutor_k # tutor k does not seem to matter? 
        self.tutor_offset = tutor_offset # flat number added to keep curve multiplier right
        self.tutor_center = tutor_center # always 0, not sure what it does exactly math math math
    
    ## placeholder
    def get_synergies(self):

        logger.debug("Begin get_synergies")
        # loop through the synergies and evaluate the search criteria first
        search_results = {"Source":[],"Target":[],"Points":[],"Tags":[],"Commander":[]}

        ## Loop 1 for card synergies

        for synergy in synergy_data.get_card_synergies():

            ### If these are not card names we may need to update this logic later to
            ## handle tag/type/etc synergies in this 
            ## could make some sets of card types and subtypes and such pretty easily

            card_names = synergy.card_names_set
            logger.debug("Card name set is: {0}".format(card_names))

            # this logger line turns feather run time from 10ms to 70ms
            #logger.debug("Checking for synergy search {0}".format(
            #    card_names
            #))

            if card_names.issubset(self.cards_set):
                logger.debug("Found potential synergy for cards {0}".format(
                    card_names
                ))                


                ## we calculate this so we know whether to use commander points or not.
                cards_cmdrs_intersection = card_names.intersection(self.commanders_set)
                if len(cards_cmdrs_intersection) > 0:
                    cmdr_synergy = True
                else:
                    cmdr_synergy = False

                if len(synergy.target) > 0:
                    #search_results.append(result)
                    #print("Searching for target match for " + str(synergy.target))
                    target_result = self.evaluate_criteria(synergy.target)
                    if target_result:
                        # no point adding results if it's scored at a 0
                        if (cmdr_synergy and synergy.cmdr_points==0) or ((not cmdr_synergy) and synergy.points==0):
                            continue
                        search_results["Source"].append(synergy.get_search_friendly_name())
                        search_results["Target"].append(synergy.get_target_friendly_name())
                        search_results["Tags"].append(synergy.tags)
                        #if card_names.issubset(self.commanders_set):
                        if cmdr_synergy:
                            search_results["Points"].append(len(target_result)*synergy.cmdr_points)
                            search_results["Commander"].append(True)
                        else:
                            search_results["Points"].append(len(target_result)*synergy.points)
                            search_results["Commander"].append(False)
                else:
                    # no point adding results if it's scored at a 0
                    if (cmdr_synergy and synergy.cmdr_points==0) or ((not cmdr_synergy) and synergy.points==0):
                        continue
                    search_results["Source"].append(synergy.get_search_friendly_name())
                    search_results["Target"].append(synergy.get_target_friendly_name())
                    search_results["Tags"].append(synergy.tags)
                    #if card_names.issubset(self.commanders_set):
                    if cmdr_synergy:
                        search_results["Points"].append(synergy.cmdr_points)
                        search_results["Commander"].append(True)
                    else:
                        search_results["Points"].append(synergy.points)
                        search_results["Commander"].append(False)
                              
        # return the list. it may be empty.
        logger.debug("End get_synergies")
        
        # compute deck tags
        self.tags_set = set.union(*search_results["Tags"])
        
        ## Loop 2 for tag synergies
        for synergy in synergy_data.get_tag_synergies():
            # check if the deck has all the tags the synergy cares about in some capacity
            if synergy.tag_names_set.issubset(self.tags_set):
                # check on requisite cards, just in case
                if synergy.card_names_set.issubset(self.cards_set):
                    # if we're here, the deck has all the requisite tags and cards
                    # and the query can be evaluated
                    query = self.evaluate_criteria(synergy.synergy_search, search_results["Tags"])
                    if query:
                        target = self.evaluate_criteria(synergy.target, search_results["Tags"])
                        if target:
                            # no point adding results if it's scored at a 0
                            if (not cmdr_synergy) and synergy.points==0:
                                continue
                            # we've got some hits
                            search_results["Source"].append(synergy.get_search_friendly_name())
                            search_results["Target"].append(synergy.get_target_friendly_name())
                            search_results["Tags"].append(synergy.tags)
                            # this might need some future logic to handle commanders. screw it for now
                            search_results["Commander"].append(False)
                            # need to get length of query/target results
                            if isinstance(query, bool):
                                query_len = 1
                            else:
                                query_len = len(query)
                            if isinstance(target, bool):
                                target_len = 1
                            else:
                                target_len = len(target)
                            search_results["Points"].append(query_len*target_len*synergy.points)
        
        ### Sorting the dataframe!
        search_results_df = pd.DataFrame.from_dict(search_results)

        # do sort by points
        search_results_df = search_results_df.sort_values(by="Points", ascending=False)      

        return search_results_df

    #Function that evaluates the presence of a given criteria in a deck
    # returns either a list of all the cards that match, or a True if 
    # a single card match
    # expects a single criteria from a synergy
    def evaluate_criteria(self, criteria:dict, tag_list=None):
        # start by checking card/commander includes
        # which are mutually exclusive with the rest due to the AND logic
        if any([i in ['CARD','COMMANDER'] for i in criteria]):
            # run the query function on the whole deck object, as the attributes are accessible this way
            # and just return what comes out
            return self.run_query(self, criteria)
        
        # check for the presence of tag queries. these use a whole different thing
        # (passed as a second argument when relevant)
        if 'TAG' in criteria:
            matches = []
            # AND logic in full effect
            tag_query = set(criteria['TAG'])
            for synergy in tag_list:
                if tag_query.issubset(synergy):
                    matches.append(synergy)
            if len(matches) > 0:
                return(matches)
            else:
                return False
        
        # if it's not a card or commander criterion, then it's a type, subtype, tag or something
        # nevertheless, it can be complicated. evaluate one card at a time for the full set of criteria
        matches = []
        for card in self.cards + self.commanders:
            if self.run_query(card, criteria):
                matches.append(card)
        # now that we've looped over all the cards, do we have any hits? report appropriately
        if len(matches) > 0:
            return matches
        else:
            return False


        return None ## Only if nothing is there to search for do we return none. 


    # helper function for evaluating queries
    @staticmethod
    def run_query(card, criteria):
        # create a helper translation dictionary for easy iteration over complex queries
        meta = {'CARD':'cards', 'COMMANDER':'commanders', 'TYPE':'types', 'SUBTYPE':'subtypes', 
            'COLOR':'colors', 'CMC':'cmc', 'TAG':'tags', 'TEXT':'oracle_text',
            'POWER':'power', 'TOUGHNESS':'toughness'}
        numeric_fields = ('CMC','TOUGHNESS', 'POWER')
        # assume a hit is happening until disproven
        match = True
        for field in criteria:
            # loop over all the AND criteria that have to be fulfilled
            for query in criteria.get(field):
                # check for any OR stuff in the individual query, denoted via |
                query = query.split('|')
                # any hits on the OR will be fine.
                local_hit = False
                for single_query in query:
                    # we could have a negation on our hands, then the query will start with !
                    # check and act accordingly. will we actually want a match or not?
                    outcome = True
                    if single_query[0] == '!':
                        single_query = single_query[1:]
                        outcome = False
                    if field in numeric_fields: ## now we just check all the numeric fields this way
                        # CMC is screwy and needs to be processed differently
                        # the first character is <, > or =, denoting the direction of thresholding
                        # the rest is the actual value
                        single_value = int(single_query[1:])

                        ## We do not want to count empty strings or tarmogoyf stuff
                        compare_value = getattr(card, meta[field])
                        try:
                            # try to convert it to an int
                            compare_value = int(compare_value)
                        except ValueError:
                            # if we get a value error, break!
                            break
                        

                        # check the appropriate thresholding for the local_hit
                        if single_query[0] == '<':
                            if (compare_value < single_value) == outcome:
                                local_hit = True
                                break
                        elif single_query[0] == '>':
                            if (compare_value > single_value) == outcome:
                                local_hit = True
                                break
                        else:
                            if (compare_value == single_value) == outcome:
                                local_hit = True
                                break
                    else:
                        # not CMC? easy life

                        if (single_query.upper() in getattr(card, meta[field])) == outcome:
                            # we have a score on one of the ORs, good enough
                            local_hit = True
                            break
                if not local_hit:
                    # none of the ORs did what they had to, match is kill
                    match = False
                    break
            if not match:
                # we have hit a snag on the matching, abort
                break
        # return our ultimate finding
        return match



    # calculates average cmc of nonlands to 2 sig figs
    def get_average_cmc(self):
        cmc_total = 0
        card_count = 0
        for card in self.cards:
            if land_type not in card.types:    
                cmc_total += card.cmc
                card_count += 1
        for card in self.commanders:
            if land_type not in card.types:
                cmc_total += card.cmc
                card_count += 1
            cmc_total += card.cmc
        return round(cmc_total / card_count, 2)

    ## a gradient curve of sorts for applying a limited range of multiplier
    ## based on the deck's curve, from 0.2 to 1.0
    def get_curve_mult_logistic(self):
        #mult = 1 / (1 + math.exp( self.curve_k * (self.curve_center - self.get_average_cmc())))
        ## new curve function
        mult = self.curve_min + (1 - self.curve_min) / (1 + math.exp( self.curve_k * (self.curve_center - self.get_average_cmc())))

        if mult < self.curve_min:
            return self.curve_min
        return mult
         

    ## get the count of total cards
    def __len__(self):
        return (len(self.commanders) + len(self.cards))

    def isValid(self):
        if self.__len__() == 100:
            return True
        else:
            return False
   
    def __repr__(self):
        return str(self.__dict__)

    ## This is no longer used
    # def get_raw_score(self):
    #     raise(NotImplementedError)
    #     self.score = 0 # start from 0
    #     ## count points for commanders then cards
    #     for card in self.commanders:
    #         if card.points is not None:
    #             self.score += card.points
        
    #     for card in self.cards:
    #         if card.points is not None:
    #             self.score += card.points
    #     # just hack it out for now
        
    #     return 0

    def get_score(self):
        ## score calculation sequence 
        total_score = 0
        total_score = total_score + self.get_synergy_score() # add synergy
        total_score = total_score * self.get_curve_mult_logistic() # multiply by curve
        ## Tutor multiplier removed in favor of tag scores.
        #total_score = total_score * self.get_tutor_mult() # multiply by tutors
        self.score = total_score
        return total_score

    ## calculates the curve adjusted score
    # updated to use synergy score as the base
    def get_curve_score(self):
        return round(self.get_synergy_score() * self.get_curve_mult_logistic(),2)

    ## return a score of synergies to add in
    def get_synergy_score(self):
        synergy_points = np.sum(self.synergies["Points"])

        #print("Synergy point total: " + str(synergy_points))
        return synergy_points # return the total!

    def __eq__(self, comparison):
        return (self.get_score() == comparison.get_score())
    
    def __gt__(self, comparison):
        return (self.get_score() > comparison.get_score())

    def __lt__(self, comparison):
        return (self.get_score() < comparison.get_score())

    ## This function returns a json representation of our deck's score
    def generate_report_json(self):
        #print(self.synergies.to_json(orient="index"))
        synergies_dict = self.synergies.to_dict('index')
        # extract the individual dicts out because it's cleaner
        output = {}
        output["synergies"] = [v for v in synergies_dict.values()]
        ## add some more fields for them to read
        output["deck_name"] = self.name
        output["curve_multiplier"] = round(self.get_curve_mult_logistic(),2)
        output["base_score"] = self.get_synergy_score()
        output["adjusted_score"] = round(self.get_score(), 2 )
        return output

    ## this function should generate a nice looking tabular report for the deck
    def generate_report(self, html=False):



        ## Subtable for summary header
        title_table = prettytable.PrettyTable()
        title_table.header = False
        title_table.title = self.name
        title_table.field_names = ["Characteristic", "Value"]
        #title_table.add_row(["Raw Score", self.get_raw_score() ])
        # Tutor score removed in favor of tag synergies.
        #title_table.add_row(["Tutor Mult", round(self.get_tutor_mult(),2)])
        title_table.add_row(["Curve Mult", round(self.get_curve_mult_logistic(),2) ])
        title_table.add_row(["Base Score", self.get_synergy_score() ])
        title_table.add_row(["Adjusted Score", round(self.get_score(), 2 )])


        # make main table
        rt = prettytable.PrettyTable()

        rt.field_names = ["Point Source", "Factor", "Points"]
        ## add the synergy values first after commanders

        logger.debug("Start pandas queries")

        ## let's replace searching for commanders with 
        # get commander only synergies
        for synergy in self.synergies.query('Commander == True').iterrows():
            ## add the commanders!
            rt.add_row( [synergy[1]["Source"], "Commander {0}".format(synergy[1]["Target"]), synergy[1]["Points"]])


        ## Do the synergies with a target value first.
        for synergy in self.synergies.query('Target != ""').iterrows():
            ## need to sort these and get the commander points first.
            if synergy[1]["Commander"] is False:
                rt.add_row( [synergy[1]["Source"], synergy[1]["Target"], synergy[1]["Points"]])

        ## do the synergies that have only a source (all cards) first
        for synergy in self.synergies.query('Target == ""').iterrows():
            ## need to sort these and get the commander points first.
            ## Let's not double print the commander synergies
            if synergy[1]["Commander"] is False:
                rt.add_row( [synergy[1]["Source"], "Cards" , synergy[1]["Points"]])
        
        logger.debug("End pandas queries")

        # final formatting
        rt.align = "l"
        rt.align["Points"] = "r"

        #print(title_table.get_string() + "\n" + rt.get_string())
        if html:
            merged_tables = title_table.get_html_string() + "\n" + rt.get_html_string()
        else:
            merged_tables = title_table.get_string() + "\n" + rt.get_string()

        return merged_tables
    
    def synergy_report(self):
        ## note, synergies get reported on at init time, so they should always
        # exist
        #output will be a table

        st = prettytable.PrettyTable()
        st.title = "{0} synergy report".format(self.name)
        st.field_names = ["Synergy", "Points"]
        # loop through them
        for synergy, result in self.synergies:
            if type(result) is list:
                st.add_row( [synergy, len(result) * synergy.points] )
            else:
                st.add_row( [synergy, synergy.points])
        
        st.align = "l"
        return st



### Loop through the deck files and create Deck objects for them and add them
### to the decks list

if __name__ == "__main__":

    for deck_file in pathlib.Path(decks_folder).iterdir():
        with open(deck_file, "r", encoding='utf-8') as deck_reader:

            ## instantiate the items we need to create the deck object
            commanders = []
            cards = []
            cmdr_read = True #this starts out true, then gets changed to false when it finds the blank line

            for line in deck_reader:
                ## Scour the lines in the deck and identify the commanders
                if line is '\n':
                    cmdr_read = False
                    # continue the loop if we find a blank line, don't need to count it or anything
                    continue
                if len(commanders) >= commander_max: ## if we have more than the configured commander max we are done
                    cmdr_read = False
                if cmdr_read:
                    # use the commanders list to append to if we are still adding commanders
                    card_list = commanders
                else:
                    card_list = cards

                ## added a fix for bad formatting cos why not-- skip over things that don't start with
                ## numbers
                if not line[0].isdigit():
                    continue

                # this is ok because you can't have more than 99 of a card
                card_count = int(line[0:2].strip(" "))
                card_name = SD.parse_card_name(line) # -1 to strip the newline

                ## Get a card value for the card

                # if we're still working on commanders...
                if cmdr_read:
                    card = card_data.cards.get(card_name.upper())
                    # if the card doesn't exist check the normal db
                    if card is None:
                        logger.error("Card {0} is not found in the database".format(card_name.upper()))
                else:
                    card = card_data.cards.get(card_name.upper())
                    if card is None:
                        logger.error("Card {0} is not found in the database".format(card_name.upper()))

                # if there are multiples we need to add all of them, e.g. for basic lands
                # or relentless rats
                for _ in range(card_count):
                    card_list.append(card)
            
            ### Now that we've got the lists, make the deck and add it to the deck list
            deck_name = deck_file.stem
            deck = Deck(deck_name, commanders, cards,
                curve_k, curve_center, curve_min,
                tutor_k, tutor_center, tutor_offset
                )
            decks.append(deck)

            ## temporary break to only do one
            #break
            #print(deck)

    ### Loop through the decks and score them. 

    
    ## Print the results
    # Sort everything!
    decks = sorted(decks, reverse=True)

    ### Loop through the decks and print them, why not

    # Let's make a nicer table
    table = prettytable.PrettyTable()
    table.title = "Deck Analysis - Total Decks: {0}".format(len(decks))

    table.field_names = ["Deck Name", "Commanders", "Avg CMC","Raw", "Adj Total"]


    for deck in decks:
        
        deck_cmdrs = ",".join([card.name for card in deck.commanders])
        ## note this currently uses get_curve_score, which is curve adjusted.
        table.add_row([
            deck.name,
            deck_cmdrs,
            '{:.2f}'.format(deck.get_average_cmc()),
            deck.get_synergy_score(),
            '{:.2f}'.format(deck.get_score())
            ]
        )

        # save the deck report
        deck_filename = deck.name + ".txt"
        deck_path = pathlib.Path(reports_folder) / deck_filename
        #deck_filepath = deck_path / deck_filename

        logger.debug("Saving deck reports")
        SD.save_report(deck.generate_report(), deck_path)
        deck_html_filename = deck.name + ".html"
        deck_html_path = pathlib.Path(reports_folder) / deck_html_filename
        SD.save_report(deck.generate_report(html=True),deck_html_path)
        logger.debug("After deck reports saved")


    # final formatting
    table.align["Deck Name"] = "l"
    table.align["Commanders"] = "l"
    table.align["Avg CMC"] = "r"
    table.align["Raw"] = "r"
    table.align["Synrg"] = "r"
    table.align["Adj Total"] = "r"

    # finish alignment
    print("\n[nfo]")
    print(table)
    print("[/nfo]")

    print("\n")


    logger.info("Completed batch procssing!")


