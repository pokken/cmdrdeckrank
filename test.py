import pathlib
import synergy_data as SD
import logging
import configparser

## Logging setup
logger = logging.getLogger(__name__)
logging.basicConfig(format="%(asctime)s | %(levelname)s |%(module)s | %(message)s", level=logging.INFO)

## test file to see if we can import the new data

synergy_file = pathlib.Path("synergy_dataset.tsv")

synergy_data = SD.SynergyData(pathlib.Path(synergy_file))

# for synergy in synergy_data:
#     #print(synergy.card_names_set)
#     if len(synergy.tag_names_set) > 0:
#         print("criteria tags are {0}".format(synergy.tag_names_set))

    #print("The synergy's tags are: {0}".format(synergy.tags))


    #print(synergy.get_search_friendly_name() + " with " + synergy.get_target_friendly_name() )


# for synergy in synergy_data.get_card_synergies():
#     print(synergy)

# for synergy in synergy_data.get_tag_synergies():
#     print(synergy)


## configparser stuff

cfg_file = "analyze.ini"
cfg = configparser.ConfigParser()
cfg.read(pathlib.Path(cfg_file))

# curve constants
curve_k = cfg['Constants'].getfloat('curve_k')
curve_center = cfg['Constants'].getfloat('curve_center')
curve_min = cfg['Constants'].getfloat('curve_min')

# tutor constants
tutor_k = cfg['Constants'].getfloat('tutor_k')
tutor_center = cfg['Constants'].getfloat('tutor_center')
tutor_offset = cfg['Constants'].getfloat('tutor_offset')

# file and folder vars
decks_folder = cfg['Files'].get('decks_folder')
reports_folder = cfg['Files'].get('reports_folder')
# magic card db
data_set_file = cfg['Files'].get('data_set_file')
# synergies
synergy_data_file = cfg['Files'].get('synergy_data_file')

# data dictionary
tags_field = cfg['Data'].get('tags_field')
tutor_tag = cfg['Data'].get('tutor_tag')
land_type = cfg['Data'].get('land_type')
