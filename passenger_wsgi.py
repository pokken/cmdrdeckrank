## This module will house all the web service related functionality

## other imports

import pathlib
import operator
import math
import prettytable
import logging
import configparser
import json
import urllib.parse
from cgi import parse_qs, escape
import unidecode # for ascii transliteration of unicode characters
## internal imports -- these happen after logging and config because otherwise they don't work :P
#import card_data
import analyze_decks
import synergy_data as SD



## Logging setup
logger = logging.getLogger(__name__)
logging.basicConfig(format="%(asctime)s | %(levelname)s |%(module)s | %(message)s")
logger.info("Logger initialized")


## configparser stuff

cfg_file = "analyze.ini"
cfg = configparser.ConfigParser()
cfg.read(pathlib.Path(cfg_file))

## web configuration
cmdrdeckrank_template = pathlib.Path(cfg['Webconfig'].get('cmdrdeckrank_template'))


## Import html!
with open(cmdrdeckrank_template, "r") as template_file:
    html = template_file.read()


## Config
deck_field = 'deck'


def application(environ, start_response):

    ##### Begin Response Handling #####
    # HTTP response code and message - default
    status = '200 OK'

    ## Always try to get the size of the content so we have it to work with
    try:
        request_body_size = int(environ.get('CONTENT_LENGTH', 0))
    except (ValueError):
        request_body_size = 0

    ## default deck report
    deck_report = "No Deck submitted yet."

    ##### Begin Request Processing ####

    logger.debug("Request method is {0}".format(environ['REQUEST_METHOD']))


    ## If someone wants json, do something completely different!
    content_type = environ.get('CONTENT_TYPE')
    logger.info("Content type is {0}".format(content_type))


    ## Now we require accept_json before we will respond with json.
    if environ.get("HTTP_ACCEPT") is not None:
        accept = environ.get("HTTP_ACCEPT")
        ### we only want to give json if they are asking for it
        if accept == "application/json":

            logger.info("HTTP_ACCEPT is {0}".format(environ.get("HTTP_ACCEPT")))

            if content_type == "application/json":
                # we're only responding if it's a post
                if environ['REQUEST_METHOD'] == "POST":

                    ### Parse the post value
                    ## only read the size!
                    raw_body = environ['wsgi.input'].read(request_body_size).decode('utf-8')
                    parsed_body = json.loads(raw_body)          
                    logger.debug("Got this json value: {0}".format(parsed_body))


                    try:
                        deck_obj = create_deck_json(parsed_body)
                    except ValueError as e:
                        logger.exception("Failed to create deck.")
                        # if we get a value error it means a card was busted!
                        status = "422 Unprocessable Entity"
                        output = json.dumps({'error_msg':str(e)})
                    else:
                        logger.info("Deck object is: {0} with {1} cards.".format(
                            deck_obj.name,
                            deck_obj.__len__()
                        ))
                        ## get our putput in a dictionary!
                        output = deck_obj.generate_report_json()
                        logger.debug(output)



                    ## display the response body --> this gets replaced with whatever we get back from the deck report
                    ## stringify the output!
                    response_body = str(output)
                    content_length = str(len(response_body.encode("utf-8")))

                    ## build headers
                    response_headers = [
                        ('Content-Type', 'application/json'),
                        ('Content-Length', content_length )
                    ]

                    # Send them to the server using the supplied function
                    start_response(status, response_headers)



                    # Return the response body. Notice it is wrapped
                    # in a list although it could be any iterable.
                    return [response_body.encode('utf-8')]
                else:
                    ### Wrong method handler
                    response_headers = [
                        ('Content-Type', 'application/json'),
                        ('Content-Length', "0" )
                    ]
                    status = "405 Method Not allowed"
                    start_response(status, response_headers)
                    return []


    #### Normal post handling, NOT json! Render the page

    if environ['REQUEST_METHOD'] == "POST":

        ## Read the body!
        #### I had to set the form to text/plain for this to work right:P multipart might have worked too.
        #### the other encoding schemes on the form were screwing stuff up.
        raw_body = environ['wsgi.input'].read(request_body_size).decode('utf-8')
        #print(raw_body)
        parsed_body = urllib.parse.parse_qs(raw_body)
        logger.debug("Parsed submit body is: {0}".format(parsed_body))

        ## Decode the respose
        #decoded_deck = decode_deck(parsed_body)

        # only parse the request if it's got some values in it
        if parsed_body.get(deck_field) is not None:
            
            try:
                deck = extract_deck(parsed_body.get(deck_field)[0])
                logger.debug("Parsed deck is: {0}".format(deck))
            except ValueError as e:
                logger.exception("Error in form field validation")
                deck_report = e
            else:
                try:
                    ## make a deck object!
                    deck_obj = create_deck(deck)
                    logger.debug("Deck object created: {0}".format(deck_obj))
                    deck_report = deck_obj.generate_report(html=True)
                except (TypeError, ValueError) as e:
                    logger.exception("Error in form field validation")
                    deck_report = e

        ##### End request handling #######

    ### we can add some stuff to detect the content-type header and not display the form then

    ## display the response body
    response_body = html % {
        'deck_report': deck_report or 'Report will be here'
    }

    # HTTP headers expected by the client
    # They must be wrapped as a list of tupled pairs:
    # [(Header name, Header value)].
    content_length = str(len(response_body.encode("utf-8")))
    response_headers = [
        ('Content-Type', 'text/html'),
        ('Content-Length', content_length)
    ]

    # Send them to the server using the supplied function
    start_response(status, response_headers)



    # Return the response body. Notice it is wrapped
    # in a list although it could be any iterable.
    return [response_body.encode('utf-8')]


## Functions!!


## convert a deck to a list of lines to parse
def extract_deck(deck:str):
    logger.debug("Attempting to split:{0}".format(deck))
    deck_list = deck.split("\r\n")
    logger.debug("Extracted {0} from input".format(deck_list))
    if len(deck_list) > 200:
        logger.error("Deck too long, {0} lines".format(len(deck_list)))
        raise ValueError("Deck too long, {0} lines".format(len(deck_list)))
     
    # if deck_list[0] == "":
    #     logger.error("Invalid format, first line is blank.")
    #     raise ValueError("Invalid format: first line is empty.")
    return deck_list


## load the deck from a json object, returns a deck
def create_deck_json(deck:dict):
    ## first get the commanders
    commanders = []
    ## if we have commanders..
    if deck.get("commanders") is not None:
        ## there will be a list of commanders
        for commander in deck.get("commanders"):
            ## get the card out of our card db
            new_commander = analyze_decks.card_data.cards.get(commander.upper())
            logger.debug("Adding {0} to the commanders list".format(new_commander) )
            if new_commander is not None:
                commanders.append(new_commander)
            else:
                raise ValueError("Card {0} does not exist.".format(commander))
    else:
        raise ValueError("Deck is invalid because it has no commanders list")

    ## now loop through the cards, it's somewhat more complex
    cards = []
    if deck.get('cards'):
        for card in deck.get("cards"):
            card_count = int(card[0:2].strip(" "))
            card_name = SD.parse_card_name(card)
            new_card = analyze_decks.card_data.cards.get(card_name.upper())
            if new_card is not None:
                logger.debug("Adding {0} to the card list".format(new_card) )
                ## add the right number of cards
                for _ in range(card_count):
                    cards.append(new_card)
            else:
                raise ValueError("Card {0} does not exist".format(card))
    
    else:
        raise ValueError("Deck is invalid because it has no cards list")

    ## Make the deck!
    #deck_name = "Test"
    deck_name = " and ".join([commander.name for commander in commanders])

    try:
        deck_obj = analyze_decks.Deck(deck_name, commanders, cards,
                analyze_decks.curve_k, analyze_decks.curve_center, analyze_decks.curve_min,
                analyze_decks.tutor_k, analyze_decks.tutor_center, analyze_decks.tutor_offset
                )
    except TypeError:
        logging.exception("Unable to capture deck {0}".format(deck_name))
        raise ValueError("There was a problem creating the deck, please clear the box and try again.")

    return deck_obj

## Load the deck object, returns a deck!
def create_deck(deck:list):
    logger.debug("Attempting to create a deck object from {0}".format(deck))
    ## vars!
    commanders = []
    cards = []
    cmdr_read = True

    deck_obj = ""
    for line in deck:
        logger.debug("Analyzing deck line {0}".format(line))

        ## If the line is empty we're no longer reading commanders, and skip over any processing of the line
        if line is '' and len(commanders) > 0:
            ## if we have found at least one commander we can safely assume that the commanders have been found
            cmdr_read = False
            continue
        if len(commanders) >= analyze_decks.commander_max: ## if we have more than the configured commander max we are done
            cmdr_read = False
        elif line is '':
            continue

        # Change the list we add to if we're not reading commanders
        if cmdr_read:
            card_list = commanders
        else:
            card_list = cards

        ## formatting checks - skip lines that don't start with a number
        if not line[0].isdigit():
            continue

        # this is ok because you can't have more than 99 of a card
        card_count = int(line[0:2].strip(" "))
        card_name = SD.parse_card_name(line)
        #card_name = unidecode.unidecode(card_name) # ascii transliteration!

        ## Get a card value for the card

        # if we're still working on commanders...
        if cmdr_read:
            card = analyze_decks.card_data.cards.get(card_name.upper())
            if card is None:
                raise ValueError("Card {0} does not exist.".format(card_name))
        else:
            card = analyze_decks.card_data.cards.get(card_name.upper())
            if card is None:
                raise ValueError("Card {0} does not exist.".format(card_name))

        # if there are multiples we need to add all of them, e.g. for basic lands
        # or relentless rats
        for _ in range(card_count):
            card_list.append(card)

    #deck_name = "Test"
    deck_name = " and ".join([commander.name for commander in commanders])

    try:
        deck_obj = analyze_decks.Deck(deck_name, commanders, cards,
                analyze_decks.curve_k, analyze_decks.curve_center, analyze_decks.curve_min,
                analyze_decks.tutor_k, analyze_decks.tutor_center, analyze_decks.tutor_offset
                )
    except TypeError:
        logging.exception("Unable to capture deck {0}".format(deck_name))
        raise ValueError("There was a problem creating the deck, please clear the box and try again.")


    return deck_obj