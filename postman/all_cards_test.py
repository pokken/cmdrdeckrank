import requests

url = "http://cmdrdeckrank.rpsands.com"

payload = """
{
    "commanders": 
    [
        "Tymna the Weaver",
        "Thrasios, Triton Hero"
    ],
    "cards": 
    [
        "1 Abrupt Decay",
        "1 Ad Nauseam",
        "1 Ancient Tomb",
        "1 Arbor Elf",
        "1 Arcane Signet",
        "1 Assassin's Trophy",
        "1 Avacyn's Pilgrim",
        "1 Bayou",
        "1 Birds of Paradise",
        "1 Bloodstained Mire",
        "1 Bloom Tender",
        "1 Brainstorm",
        "1 Breeding Pool",
        "1 Carpet of Flowers",
        "1 Chain of Vapor",
        "1 Chrome Mox",
        "1 City of Brass",
        "1 Command Tower",
        "1 Copy Artifact",
        "1 Counterspell",
        "1 Cyclonic Rift",
        "1 Dark Confidant",
        "1 Dark Ritual",
        "1 Deathrite Shaman",
        "1 Delay",
        "1 Demonic Consultation",
        "1 Demonic Tutor",
        "1 Dispel",
        "1 Dovin's Veto",
        "1 Dramatic Reversal",
        "1 Drown in the Loch",
        "1 Elves of Deep Shadow",
        "1 Enlightened Tutor",
        "1 Exotic Orchard",
        "1 Fellwar Stone",
        "1 Flooded Strand",
        "1 Flusterstorm",
        "1 Forbidden Orchard",
        "1 Force of Will",
        "1 Gemstone Caverns",
        "1 Gilded Drake",
        "1 Gitaxian Probe",
        "1 Grim Monolith",
        "1 Imperial Seal",
        "1 Isochron Scepter",
        "1 Jace, Wielder of Mysteries",
        "1 Lim-Dûl's Vault",
        "1 Mana Confluence",
        "1 Mana Crypt",
        "1 Mana Drain",
        "1 Mana Vault",
        "1 Marsh Flats",
        "1 Mental Misstep",
        "1 Misty Rainforest",
        "1 Morphic Pool",
        "1 Mox Diamond",
        "1 Mystic Remora",
        "1 Mystical Tutor",
        "1 Necropotence",
        "1 Negate",
        "1 Noble Hierarch",
        "1 Notion Thief",
        "1 Noxious Revival",
        "1 Nurturing Peatland",
        "1 Pact of Negation",
        "1 Plunge into Darkness",
        "1 Polluted Delta",
        "1 Ponder",
        "1 Preordain",
        "1 Priest of Titania",
        "1 Rhystic Study",
        "1 Savannah",
        "1 Scalding Tarn",
        "1 Sensei's Divining Top",
        "1 Silence",
        "1 Smothering Tithe",
        "2 Snow-Covered Island",
        "1 Sol Ring",
        "1 Swan Song",
        "1 Sylvan Library",
        "1 Tainted Pact",
        "1 Tarnished Citadel",
        "1 Thassa's Oracle",
        "1 Timetwister",
        "1 Toxic Deluge",
        "1 Tropical Island",
        "1 Tundra",
        "1 Underground Sea",
        "1 Vampiric Tutor",
        "1 Veil of Summer",
        "1 Verdant Catacombs",
        "1 Waterlogged Grove",
        "1 Watery Grave",
        "1 Windfall",
        "1 Windswept Heath",
        "1 Wooded Foothills",
        "1 Yawgmoth's Will"
    ]
}
"""

payload = payload.encode("utf-8")

## Note that it expects a utf-8 encoded payload

headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data = payload)

## we do not get a utf-8 encoded response by default, which I think is normal? Not sure, might be
## we should be sending a utf-8 encoded response. 

print(response.text)
