# Module that can be imported

import pandas as pd
import numpy as np
import csv
import pathlib
import json # used for loading list
import logging
import unidecode # for ascii transliteration of card names

# setup logger
logger = logging.getLogger(__name__)
#logger.setLevel(logging.DEBUG) ## use this to test just this entry's debug if needed

## Helper functions

## this function is needed because parsing card names is frigging annoying.
# added remove_card_count flag so we can use this in tags too.
def parse_card_name(card_name:str,remove_card_count=True):
    # trim the number
    # it is okay to just strip the first two characters. if it leaves a space it'll get got by the whitespace trimmer.
    if remove_card_count:
        card_name = card_name[2:]
    # trim split cards
    if "/" in card_name:
        card_name = card_name[0:card_name.index("/")]
    # trim newlines
    card_name = card_name.strip('\n')
    # trim whitespace
    card_name = card_name.lstrip().rstrip()
    card_name = unidecode.unidecode(card_name) # strip characters that don't belong
    # how do we fix lim dul's vault?
    return card_name.upper()

def save_report(report:str, output:pathlib.Path, append=False):
    if append:
        mode = "a"
    else:
        mode = "w"
    with open(output, mode, encoding='utf-8') as f:
        if append:
            f.write("\n")
        f.write(report)


## end helper functions



# may need to list synergy types but let's try not to

## The class that holds a dataset of card synergies
class SynergyData:

    ## data dictionary
    synergy_field = "Synergy"
    target_field = "Target"
    category_field = "Category"
    points_field = "Points"
    cmdr_points_field = "Commander_Points"
    tags_field = "Tags"



    def __init__(self, data_file:pathlib.Path):
        self.data_file = data_file
        self.synergies = [] # blank list of synergies
    
        reader = pd.read_table(data_file, encoding='utf-8')
        for entry in reader.index:
            synergy = self.parse_synergy(reader.loc[entry,self.synergy_field])
            target = self.parse_synergy(reader.loc[entry,self.target_field])
            points = reader.loc[entry,self.points_field]
            cmdr_points = reader.loc[entry,self.cmdr_points_field]
            tags = reader.loc[entry,self.tags_field]
            if tags is not np.nan: 
                # if it's not NAN then split it into a list
                tags = set([i.strip().upper() for i in tags.split(",")])
            else:
                tags = set()
            
            self.synergies.append(
                Synergy(
                    synergy,
                    target,
                    points,
                    cmdr_points,
                    tags
                )
            )
        logger.info("Loaded {0} card synergies.".format(str(len(self.synergies))))   
        
        #print(self.synergies)
    
    ## Card iterator
    def get_card_synergies(self):
        # return a list of synergies filtering out the ones with no cards
        return iter([synergy for synergy in self.synergies if len(synergy.card_names_set)>0 and len(synergy.tag_names_set) is 0 ])
        # make sure this excludes ones that have tags

    # tag iterator
    def get_tag_synergies(self):
        # now filthily hacked to feature stuff that isn't just tag queries, but e.g. also text to text
        return iter( [synergy for synergy in self.synergies if len(synergy.tag_names_set)>0 or len(synergy.card_names_set) is 0 ]   )


    ## return an iterator on our list of synergies
    def __iter__(self):

        return iter(self.synergies)

        #return iter(self.synergies)


    # function for parsing a synergy string into the correct dict
    def parse_synergy(self, synergy:str):
        
        logger.debug("Trying to parse: {0}".format(synergy))
        ## if our synergy is blank return a blank dict
        if synergy is np.nan: 
            synergy = None
            return dict()
        else:
        
            synergy_dict = json.loads(synergy, encoding='utf-8')
            ### updated this to uppercase everything hooray!
            ## updated to use parse_card_name!!! as nature intended. 
            synergy_dict = {k.upper(): [parse_card_name(val,remove_card_count=False) for val in v ] for k, v in synergy_dict.items()}
            logger.debug("Returning {0}".format(synergy_dict))
            return synergy_dict

## An individual synergy, with various elements
class Synergy:

    # data dictionary for synergy
    card_field = 'CARD'
    tag_field = "TAG"

    def __init__(self, synergy_search:dict, target:dict, points:int, cmdr_points:int, tags:list):
        self.synergy_search = synergy_search # the items we will search for; dict of {type:list of values}
        self.target = target # the items we will search for; dict of {type:list of values}
        self.points = points # the points each match is worth
        self.cmdr_points = cmdr_points # int rep of the points if one of the cards is your commander
        
        self.tags = tags # the tags for filtering if needed- NOT the criteria tags


        ## add a set of all the cards in the synergy
        self.card_names_set = self.get_card_names_set()
        ## add another set indexer for all the tags *in the criteria*
        self.tag_names_set = self.get_tag_names_set()
        logger.debug("Created synergy {0} with {1}".format(self.get_search_friendly_name(),
            self.get_target_friendly_name()
         ) )



    def __repr__(self):
        return str(self.__dict__)

    def get_card_names_set(self):

        search_val = self.synergy_search.get(self.card_field)
        search_set = set()
        logger.debug(search_val)
        if search_val is not None:
            search_set = set([unidecode.unidecode(i.upper()) for i in search_val])
            logger.debug("Search set is {0}".format(
                search_set
            ))

        target_val = self.target.get(self.card_field)
        target_set = set()
        if target_val is not None:
            target_set = set([unidecode.unidecode(i.upper()) for i in target_val])
            logger.debug("Target set is {0}".format(
                target_set
            ))
        card_names = search_set | target_set
        logger.debug("card names {0}".format(card_names))
        return card_names

    # create a set of all the tags that are in synergy CRITERIA
    def get_tag_names_set(self):
        search_val = self.synergy_search.get(self.tag_field)
        search_set = set()
        if search_val is not None:
            search_set = set([i.upper() for i in search_val])
            logger.debug("Search set is {0}".format(
                search_set
            ))
        target_val = self.target.get(self.tag_field)
        target_set = set()
        if target_val is not None:
            target_set = set([i.upper() for i in target_val])
            logger.debug("Target set is {0}".format(
                target_set
            ))
        tag_names = search_set | target_set
        return tag_names


    ## get a friendly name of a criteria dictionary, e.g. synergy_serach or target
    def get_search_friendly_name(self):
        return self.get_friendly_name(self.synergy_search)
    
    ## get a friendly name of a criteria dictionary, e.g. synergy_serach or target
    def get_target_friendly_name(self):
        return self.get_friendly_name(self.target)

    ## transform a criteria dictionary into english
    @staticmethod
    def get_friendly_name(criteria:dict):
        ## add some empty dict checking
        if len(criteria)>0:
            logger.debug("Attempting to transform {0}".format(criteria))
            ## more robust name discovery:
            name_list = []
            for criteria_name,criteria_value in criteria.items():
                name_list.append("{0}:{1}".format(
                    criteria_name,
                    ",".join(criteria_value)
                ))

            friendly_name = ",".join([name for name in name_list])
            return friendly_name
        else:
            ## return a blank string if it's empty
            return ""

    ## Defining a custom __eq__ method so we can test to see if a synergy contains a card. Makes subsetting easy!
    # this method will compare differently depending on of something compared is a card or a synergy
    def __eq__(self, comparison):
        ### compare on the Str value if it's a synergy
        if type(comparison) is Synergy:
            return self.__str__() == comparison.__str__()
        ### Synergies compare equal to MagicCards if they have a card in common
        elif type(comparison) is MagicCard:
            search_comp = (comparison.name in self.get_search_friendly_name() )
            logger.info("Compared {0} to {1} and got {2}".format(
                comparison.name,
                self.get_search_friendly_name(),
                search_comp

            ))
            target_comp = ( comparison.name in self.get_target_friendly_name() )
            logger.info("Compared {0} to {1} and got {2}".format(
                comparison.name,
                self.get_target_friendly_name(),
                target_comp

            ))
            return search_comp or target_comp

    ## A pretty and simplified string representation for use in reports
    def __str__(self):
        

        ### Man this is some hideous syntax getting the list out of he tiny dictionary. It's
        ## possible dictionaries are not the right format for that or I just do not understand
        ## something fundamental about how to get a single item out of a dict :P

        name_list = list(self.synergy_search.values())[0]
        ss_name = ",".join([name for name in name_list])
        if self.target:
            target_list = list(self.target.values())[0]
            target_name = ",".join([name for name in target_list])
        else:
            target_name = ""
        #name = synergy_search_name + " synergizes with " + target_name
        return ss_name + " with " + target_name

#synergies = CardSynergies(pathlib.Path(data_file))

## merged MagicCard and CardData!

### This file houses the entire list of magic cards for running curve analysis
### and other data (e.g. card types)

## This module will have two dictionaries of magic cards indexed by name
# cmdr_cards == a list of all pointed commander cards

## This is a magic card with only the data elements we need
class MagicCard:
    def __init__(self, name:str, cmc:int, types:list, subtypes:list,
        oracle_text:str, colors:list, power:int, toughness:int, supertypes:list,
        mana_cost:str, points: int=None, category: str=None, tags: list=None):

        self.name = name.upper()
        self.lazy_name = unidecode.unidecode(self.name)
        self.cmc = cmc
        self.types = [card_type.upper() for card_type in types]
        self.subtypes = [subtype.upper() for subtype in subtypes]
        self.oracle_text = oracle_text.upper()
        self.colors = [color.upper() for color in colors]
        self.power = power
        self.toughness = toughness
        self.supertypes = [supertype.upper() for supertype in supertypes]
        self.points = points
        self.mana_cost = mana_cost
        if category is not None:
            self.category = category.upper()
        else:
            self.category = category
        if tags is not None:
            self.tags = [tag.upper() for tag in tags]
            #print(self.tags)
        else:
            self.tags = [] # this should never be done cos we search in it

        logger.debug("Created card: {0}".format(self.__dict__))
    
    def __repr__(self):
        return str(self.__dict__)

    ## these are only implemented for points sorting, may not work
    def __gt__(self, comparison):
        ## This whole None stuff is a stupid crutch I should fix it so the program
        ## doesn't depend on Points being None, should use 0 as the default.
        if self.points is None:
            return False
        if comparison.points is None:
            return True
        return self.points > comparison.points
    

    ### We're going to modify this so that it
    ### works for both comparisons of cards and strings
    def __eq__(self, comparison):

        ## first if they're giving us a string we match by
        # our name! useful eh?
        if type(comparison) == str:
            #print("Comparing {0} to {1}".format(self.name, comparison.upper()))
            return comparison.upper() == self.name

        # if it's not a string we can do the math stuff
        if self.points is None:
            return False
        if comparison.points is None:
            return False
        return self.points == comparison.points




class CardData:


    # this set will hold all our cards. This is a dictionary with the name as the key.
    # we're using two separate dicts because it's just faster to be able to .get by name
    cards = {}
    # This holds the cards specifically with commander points
    cmdr_cards = {}

    # Data dictionary
    all_name_field = "name"
    all_cmc_field = "convertedManaCost"
    all_types_field = "types"
    all_subtypes_field = "subtypes"
    all_colors_field = "colors"
    all_oracle_text_field = "text"
    all_power_field = "power"
    all_toughness_field = "toughness"
    all_supertype_field = "supertypes"
    all_mana_cost_field = "manaCost"

    ## data repositories for custom metadata
    card_data = {}
    cmdr_card_data = {}

    def __init__(self, all_cards_file:pathlib.Path):
        
        ### read the master list of cards and populate a dataset merged with
        ### our proprietary metadata (points, etc.)

        with open(all_cards_file, "r", encoding='utf-8') as data_file:
            reader = csv.DictReader(data_file, delimiter=',', quotechar='"')
            for entry in reader:


                ## Parse the CMC correctly for use by the subsequent functions
                try:
                    card_cmc = float(entry.get(self.all_cmc_field))
                except ValueError:
                    card_cmc = 0

 
                card = MagicCard(
                    entry.get(self.all_name_field),
                    card_cmc,
                    [card_type for card_type in entry.get(self.all_types_field).split(",")],
                    [sub_type for sub_type in entry.get(self.all_subtypes_field).split(",")],
                    entry.get(self.all_oracle_text_field),
                    [color for color in entry.get(self.all_colors_field).split(",")],
                    entry.get(self.all_power_field),
                    entry.get(self.all_toughness_field),
                    [supertype for supertype in entry.get(self.all_supertype_field).split(",")],
                    entry.get(self.all_mana_cost_field)

                )
                #self.cards[card.name] = card
                self.cards[card.lazy_name] = card
        ## logging statements
        #logger.info("Loaded {0} cards to the commander dictionary.".format(str(len(self.cmdr_cards))))
        logger.info("Loaded {0} cards to the card dictionary.".format(str(len(self.cards))))

