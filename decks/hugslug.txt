1 Grothama, All-Devouring

1 Concordant Crossroads
1 Hall of the Bandit Lord
1 Lightning Greaves
1 Surrak, the Hunt Caller
1 Exploration
1 Bear Umbra
1 Sword of Feast and Famine
1 Nature's Will
1 Vigor
1 Silvos, Rogue Elemental
1 Spearbreaker Behemoth
1 Mossbridge Troll
1 Pathbreaker Ibex
1 Ghalta, Primal Hunger
1 Selvala, Heart of the Wilds
1 Somberwald Sage
1 Stonehoof Chieftain
1 Temur Sabertooth
1 Traverse the Outlands
1 Arbor Elf
1 Cultivate
1 Kodama's Reach
1 Nissa's Pilgrimage
1 Overgrowth
1 Wild Growth
1 Utopia Sprawl
1 Cauldron of Souls
1 Reliquary Tower
1 Praetor's Counsel
1 Thought Vessel
1 Sol Ring
1 Zendikar Resurgent
1 Joraga Treespeaker
1 Beast Within
1 Reclamation Sage
1 Heroic Intervention
1 Strionic Resonator
1 Rishkar's Expertise
1 Greater Good
1 Hunted Troll
1 Nylea, God of the Hunt
1 Rhonas the Indomitable
1 Dragon Throne of Tarkir
1 High Market
1 Sekki, Seasons' Guide
1 Mortal's Resolve
1 Withstand Death
1 Vernal Bloom
1 Garruk, Primal Hunter
1 Terastodon
1 Skyshroud Claim
1 Mosswort Bridge
1 Nykthos, Shrine to Nyx
1 Myriad Landscape
1 Miren, the Moaning Well
1 Nature's Lore
1 Asceticism
1 Wayward Swordtooth
1 Blanchwood Armor
1 Strata Scythe
29 Forest
1 Druid's Call
1 Skullwinder
1 Primal Bellow
1 Invigorate
1 Realm Seekers
1 Ranger's Path
1 Berserk
1 Not of This World
1 Broodhatch Nantuko
1 Saber Ants
