1 Nahiri, the Lithomancer

1 Static Orb
1 Winter Orb
1 Storage Matrix
1 Portcullis
1 Sol Ring
1 Mind Stone
1 Fellwar Stone
1 Everflowing Chalice
1 Mana Vault
1 Worn Powerstone
1 Thran Dynamo
1 Gilded Lotus
1 Aura of Silence
1 Glowrider
1 Lodestone Golem
1 Sphere of Resistance
1 Thalia, Guardian of Thraben
1 Thorn of Amethyst
1 Vryn Wingmare
1 Hushwing Gryff
1 Torpor Orb
1 Cursed Totem
1 Damping Matrix
1 Mana Web
1 Orb of Dreams
1 Aven Mindcensor
1 Trading Post
1 Luminarch Ascension
1 Armageddon
1 Catastrophe
1 Elspeth Tirel
1 Elspeth, Knight-Errant
1 Elspeth, Sun's Champion
1 Hokori, Dust Drinker
1 Ancient Tomb
1 Buried Ruin
1 Crackdown
1 Meekstone
1 Flagstones of Trokair
1 Dust Bowl
1 Strip Mine
1 Humility
1 Karmic Justice
1 Emeria, the Sky Ruin
1 Guardian Idol
1 Prismatic Lens
1 Enlightened Tutor
1 Thunder Totem
1 Grafdigger's Cage
1 Austere Command
1 Day of Judgment
1 Phyrexian Rebirth
1 Rout
1 Wrath of God
1 Argentum Armor
1 Godsend
1 Sword of Feast and Famine
1 Sword of Fire and Ice
1 Sword of Kaldra
1 Lightning Greaves
1 Moonsilver Spear
1 Helm of Kaldra
1 Shield of Kaldra
1 Strata Scythe
1 Umezawa's Jitte
1 Sun Titan
1 Path to Exile
1 Swords to Plowshares
1 Intrepid Hero
1 Oblivion Ring
1 Banishing Light
28 Plains
