1 Zetalpa, Primal Dawn

1 Hushwing Gryff
1 Aven Mindcensor
1 Linvala, Keeper of Silence
1 Avacyn, Angel of Hope
1 Sun Titan
1 Stonehewer Giant
1 Stoneforge Mystic
1 Burnished Hart
1 Solemn Simulacrum
1 Sword of Feast and Famine
1 Sword of Fire and Ice
1 Sword of Light and Shadow
1 Sword of the Animist
1 Umezawa's Jitte
1 Mask of Memory
1 Loxodon Warhammer
1 Sigarda's Aid
1 Strata Scythe
1 Lightning Greaves
1 Swiftfoot Boots
1 Wayfarer's Bauble
1 Enlightened Tutor
1 Path to Exile
1 Swords to Plowshares
1 Grasp of Fate
1 Darksteel Mutation
1 Puresteel Paladin
1 Open the Armory
1 Recruiter of the Guard
1 Dispatch
1 Ugin, the Spirit Dragon
1 Land Tax
1 Wrath of God
1 Emeria Shepherd
1 Containment Priest
1 Sol Ring
1 Mind Stone
1 Commander's Sphere
1 Aura of Silence
1 Council's Judgment
1 Expedition Map
1 Weathered Wayfarer
1 Emeria, the Sky Ruin
1 Secluded Steppe
1 Drifting Meadow
1 Rogue's Passage
1 Buried Ruin
1 Inventors' Fair
1 Strip Mine
1 Myriad Landscape
25 Plains
1 Indomitable Archangel
1 Sram, Senior Edificer
1 Austere Command
1 Rout
1 Return to Dust
1 Mana Crypt
1 Mana Vault
1 Grim Monolith
1 Thran Dynamo
1 Ancient Tomb
1 Worn Powerstone
1 Selfless Spirit
1 Odric, Lunarch Marshal
1 Frontline Medic
1 Worldslayer
1 Day of Judgment
1 Tragic Arrogance
1 Rogue's Gloves
1 Hall of the Bandit Lord
1 Angel of Jubilation
1 Grafted Exoskeleton
1 Inquisitor's Flail
1 Endless Horizons
1 Kor Cartographer
