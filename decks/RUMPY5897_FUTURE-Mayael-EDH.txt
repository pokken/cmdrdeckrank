1 Mayael the Anima

1 Battlefield Forge
1 Naya Charm
1 Xenagos, God of Revels
1 Wind-Scarred Crag
1 Magmatic Force
1 Command Tower
1 Avenger of Zendikar
1 Angel of Serenity
5 Plains
6 Forest
1 Atarka, World Render
5 Mountain
1 Quicksilver Amulet
1 Molten Primordial
1 Fires of Yavimaya
1 Archetype of Endurance
1 Cultivate
1 Tyrant's Familiar
1 Jungle Shrine
1 Mosswort Bridge
1 Temple of Triumph
1 Terastodon
1 Temple of Abandon
1 Sol Ring
1 Commander's Sphere
1 Explosive Vegetation
1 Blossoming Sands
1 Sylvan Caryatid
1 Gisela, Blade of Goldnight
1 Seedborn Muse
1 Rings of Brighthearth
1 Talisman of Impulse
1 Talisman of Unity
1 Avatar of Slaughter
1 Boros Signet
1 Worldspine Wurm
1 Omen Machine
1 Giant Adephage
1 Fervor
1 Platinum Emperion
1 Void Winnower
1 Illusionist's Bracers
1 Boros Charm
1 Blazing Archon
1 Congregation at Dawn
1 Kodama's Reach
1 Balefire Dragon
1 Worldly Tutor
1 Swords to Plowshares
1 Mirri's Guile
1 Decimate
1 Hull Breach
1 Karplusan Forest
1 Brushland
1 Clifftop Retreat
1 Rootbound Crag
1 Deceiver of Form
1 Ruric Thar, the Unbowed
1 Scourge of the Throne
1 Selvala's Stampede
1 Berserk
1 Birds of Paradise
1 Beast Within
1 Rout
1 Path to Exile
1 Mirari's Wake
1 Sensei's Divining Top
1 Siege Behemoth
1 Cinder Glade
1 Game Trail
1 Canopy Vista
1 Fortified Village
1 Elderscale Wurm
1 Nature's Claim
1 Avacyn, Angel of Hope
1 Thunderfoot Baloth
1 Search for Tomorrow
1 Nature's Lore
1 Temple Garden
1 Etali, Primal Storm
1 Rhythm of the Wild
1 Hydra Omnivore
1 Austere Command
1 Verdant Force
1 Terminus
1 Stomping Ground
