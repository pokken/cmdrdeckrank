1 Tymna the Weaver
1 Ludevic, Necro-Alchemist

1 Arid Mesa
1 Blinkmoth Nexus
1 Blood Crypt
1 Bloodstained Mire
1 Cascade Bluffs
1 City of Brass
1 Command Tower
1 Exotic Orchard
1 Fetid Heath
1 Flooded Strand
1 Glimmervoid
1 Godless Shrine
1 Graven Cairns
1 Hallowed Fountain
1 Inkmoth Nexus
2 Island
1 Izzet Boilerworks
1 Mana Confluence
1 Marsh Flats
1 Mishra's Workshop
1 Mountain
1 Mystic Gate
1 Orzhov Basilica
1 Plains
1 Polluted Delta
1 Reflecting Pool
1 Rugged Prairie
1 Sacred Foundry
1 Scalding Tarn
1 Spire of Industry
1 Steam Vents
1 Swamp
1 Unclaimed Territory
1 Watery Grave
1 Arcane Denial
1 Arcbound Ravager
1 Baleful Strix
1 Bomat Courier
1 Chaos Warp
1 Chief of the Foundry
1 Crackling Doom
1 Cranial Plating
1 Daretti, Ingenious Iconoclast
1 Dispatch
1 Dockside Extortionist
1 Dowsing Dagger
1 Etched Champion
1 Etherium Sculptor
1 Foundry Inspector
1 Goblin Engineer
1 Goblin Welder
1 Gold Myr
1 Hangarback Walker
1 Iron Myr
1 Izzet Signet
1 Jhoira's Familiar
1 Leaden Myr
1 Living Death
1 Lodestone Golem
1 Master of Etherium
1 Memnite
1 Memory Lapse
1 Mox Amber
1 Mox Opal
1 Mystic Forge
1 Nahiri, the Harbinger
1 Ornithopter
1 Orzhov Signet
1 Phyrexian Metamorph
1 Pyroblast
1 Sai, Master Thopterist
1 Scrap Trawler
1 Scrapyard Recombiner
1 Sculpting Steel
1 Sensei's Divining Top
1 Silver Myr
1 Smuggler's Copter
1 Sol Ring
1 Spellskite
1 Springleaf Drum
1 Steel Overseer
1 Sword of the Meek
1 Sword of War and Peace
1 Swords to Plowshares
1 Teshar, Ancestor's Apostle
1 Tezzeret, Agent of Bolas
1 Tezzeret, Master of the Bridge
1 Thopter Assembly
1 Thopter Foundry
1 Thoughtcast
1 Time Sieve
1 Tithe
1 Ugin, the Ineffable
1 Vault Skirge
1 Walking Atlas
1 Walking Ballista
1 Winds of Abandon