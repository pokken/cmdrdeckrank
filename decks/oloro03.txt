1 Oloro, Ageless Ascetic

1 Alhammarret's Archive
1 Karlov of the Ghost Council
1 Well of Lost Dreams
1 Toxic Deluge
1 Divinity of Pride
1 Drogskol Reaver
1 Consecrated Sphinx
1 Pristine Talisman
1 Cyclonic Rift
1 Ajani's Pridemate
1 Archangel of Thune
1 Gilded Drake
1 Swords to Plowshares
1 Path to Exile
1 Utter End
1 Phyrexian Arena
1 Chromatic Lantern
1 Wall of Reverence
1 Bloodgift Demon
1 Sunscorch Regent
1 Sol Ring
1 Thought Vessel
1 Azorius Signet
1 Dimir Signet
1 Orzhov Signet
1 Commander's Sphere
1 Counterspell
1 Mortify
1 Notion Thief
1 Demonic Tutor
1 Lim-Dûl's Vault
1 Blind Obedience
1 Rhystic Study
1 Phyrexian Reclamation
1 Darksteel Mutation
1 Teferi, Temporal Archmage
1 Elspeth, Sun's Champion
1 Forbid
1 Phyrexian Metamorph
1 Clever Impersonator
1 Ghostly Prison
1 Propaganda
1 Sphere of Safety
1 Enlightened Tutor
1 Vampiric Tutor
1 Mystical Tutor
1 Crawlspace
1 Command Tower
1 Hallowed Fountain
1 Godless Shrine
1 Watery Grave
1 Arcane Sanctum
1 Temple of Deceit
1 Temple of Enlightenment
1 Temple of Silence
1 Drowned Catacomb
1 Isolated Chapel
1 Glacial Fortress
1 Prairie Stream
1 Sunken Hollow
7 Plains
3 Island
4 Swamp
1 Reliquary Tower
1 Mother of Runes
1 Weathered Wayfarer
1 Rout
1 No Mercy
1 Necropotence
1 Grasp of Fate
1 Sun Droplet
1 Supreme Verdict
1 Wrath of God
1 Urborg, Tomb of Yawgmoth
1 Dismal Backwater
1 Scoured Barrens
1 Tranquil Cove
1 Jwar Isle Refuge
1 Sejiri Refuge
1 Baleful Strix
1 Bribery
1 Vampire Nighthawk
1 Reflecting Pool
1 Magister Sphinx
1 Sphinx of the Steel Wind
1 Righteous Cause
1 Rhox Faithmender
1 Aurification
