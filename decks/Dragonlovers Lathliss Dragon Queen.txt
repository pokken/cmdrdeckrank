1 Lathliss, Dragon Queen

1 Akoum Hellkite
1 Ancient Hellkite
1 Archwing Dragon
1 Balefire Dragon
1 Bogardan Hellkite
1 Demanding Dragon
1 Dragon Egg
1 Dragon Hatchling
1 Dragon Whelp
1 Drakuseth, Maw of Flames
1 Dream Pillager
1 Furyborn Hellkite
1 Glorybringer
1 Hellkite Charger
1 Hoard-Smelter Dragon
1 Kilnmouth Dragon
1 Knollspine Dragon
1 Mana-Charged Dragon
1 Moonveil Dragon
1 Opportunistic Dragon
1 Runehorn Hellkite
1 Ryusei, the Falling Star
1 Sarkhan's Whelp
1 Scourge of Kher Ridges
1 Scourge of the Throne
1 Scourge of Valkas
1 Skyline Despot
1 Slumbering Dragon
1 Spawn of Thraxes
1 Steel Hellkite
1 Stormbreath Dragon
1 Thunderbreak Regent
1 Thundermaw Hellkite
1 Tyrant of Valakut
1 Utvara Hellkite
1 Verix Bladewing
1 Caged Sun
1 Darksteel Ingot
1 Dragon's Hoard
1 Dragonlord's Servant
1 Dragonspeaker Shaman
1 Extraplanar Lens
1 Gauntlet of Power
1 Hazoret's Monument
1 Ruby Medallion
1 Sol Ring
1 Solemn Simulacrum
1 Sarkhan, Dragonsoul
1 Sarkhan, Fireblood
1 Sarkhan the Masterless
1 Crucible of Fire
1 Dragon Tempest
1 Kindred Charge
1 Sarkhan's Unsealing
1 Vanquisher's Banner
1 Sarkhan's Triumph
1 Anger
1 Earthquake
1 Fireball
1 Indomitable Creativity
1 Lightning Greaves
1 Magmaquake
1 Panharmonicon
1 Possibility Storm
1 Crucible of the Spirit Dragon
1 Haven of the Spirit Dragon
1 Nykthos, Shrine to Nyx
1 Path of Ancestry
1 Reliquary Tower
1 Spinerock Knoll
1 Temple of the False God
1 Valakut, the Molten Pinnacle
27 Mountain