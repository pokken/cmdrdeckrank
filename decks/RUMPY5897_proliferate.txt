1 Ghired, Conclave Exile

1 Garruk's Packleader
1 Sakura-Tribe Elder
1 Harmonize
1 Beast Within
1 Sol Ring
1 Elemental Bond
1 Cinder Glade
1 Exotic Orchard
3 Plains
2 Mountain
5 Forest
1 Command Tower
1 Krosan Verge
1 Idol of Oblivion
1 Kiki-Jiki, Mirror Breaker
1 Selfless Spirit
1 Bramble Sovereign
1 Flameshadow Conjuring
1 Wood Elves
1 Sylvan Library
1 Iroas, God of Victory
1 Wheel of Fortune
1 Generous Gift
1 Twinflame
1 Canopy Vista
1 Pathbreaker Ibex
1 Avacyn, Angel of Hope
1 Swords to Plowshares
1 Path to Exile
1 Ohran Frostfang
1 Eternal Witness
1 Heroic Intervention
1 Archangel of Thune
1 Chord of Calling
1 Flooded Strand
1 Bloodstained Mire
1 Wooded Foothills
1 Windswept Heath
1 Marsh Flats
1 Scalding Tarn
1 Verdant Catacombs
1 Arid Mesa
1 Misty Rainforest
1 Sacred Foundry
1 Stomping Ground
1 Temple Garden
1 Mana Crypt
1 Ogre Battledriver
1 Anointed Procession
1 Parallel Lives
1 Helm of the Host
1 Reconnaissance
1 City of Brass
1 Mana Confluence
1 Reflecting Pool
1 Duergar Hedge-Mage
1 Thunderfoot Baloth
1 Blasphemous Act
1 Enlightened Tutor
1 Aurelia, the Warleader
1 Vigor
1 Cultivate
1 Kodama's Reach
1 Life's Legacy
1 Splinter Twin
1 Odric, Master Tactician
1 Finale of Devastation
1 Eladamri's Call
1 Teferi's Protection
1 Ancient Tomb
1 Hall of the Bandit Lord
1 Kalonian Hydra
1 Taiga
1 Savannah
1 Plateau
1 Path of Ancestry
1 Beastmaster Ascension
1 Joraga Treespeaker
1 Gisela, Blade of Goldnight
1 Birthing Pod
1 Blade of Selves
1 Subjugator Angel
1 Rishkar's Expertise
1 Flamerush Rider
1 Doubling Season
1 Greater Good
1 Nissa's Pilgrimage
1 Search for Tomorrow
1 Farhaven Elf
1 Farseek
1 Nature's Lore
1 Three Visits
