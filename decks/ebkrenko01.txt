1 Krenko, Mob Boss

30 Snow-Covered Mountain
1 Valakut, the Molten Pinnacle
1 Cavern of Souls
1 Mana Crypt
1 Lotus Petal
1 Chrome Mox
1 Mox Diamond
1 Basalt Monolith
1 Grim Monolith
1 Sol Ring
1 Mana Vault
1 Thornbite Staff
1 Lightning Greaves
1 Umbral Mantle
1 Skullclamp
1 Gauntlet of Might
1 Staff of Domination
1 Phyrexian Altar
1 Ashnod's Altar
1 Rings of Brighthearth
1 Sword of the Paruns
1 Swiftfoot Boots
1 Aggravated Assault
1 Mana Echoes
1 In the Web of War
1 Shared Animosity
1 Fervor
1 Goblin Bombardment
1 Blood Moon
1 Warp World
1 Seize the Day
1 Wheel of Fate
1 Wheel of Fortune
1 Reiterate
1 Battle Hymn
1 Brightstone Ritual
1 Koth of the Hammer
1 Goblin Sharpshooter
1 Purphoros, God of the Forge
1 Goblin Bushwhacker
1 Goblin Matron
1 Kiki-Jiki, Mirror Breaker
1 Zealous Conscripts
1 Chancellor of the Forge
1 Lightning Crafter
1 Changeling Berserker
1 Legion Loyalist
1 Imperial Recruiter
1 Skirk Prospector
1 Goblin Recruiter
1 Moggcatcher
1 Ib Halfheart, Goblin Tactician
1 Goblin Warchief
1 Goblin King
1 Capricious Efreet
1 Generator Servant
1 Goblin Chieftain
1 Ancient Tomb
1 Temple of the False God
1 Anger
1 Darksteel Citadel
1 Great Furnace
1 Dualcaster Mage
1 Heat Shimmer
1 Twinflame
1 Faithless Looting
1 Outpost Siege
1 Impact Tremors
1 Carnage Altar
1 Mox Opal
