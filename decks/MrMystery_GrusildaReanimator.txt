1 Grusilda, Monster Masher

//deck-1
1 Abrade
1 Ancient Tomb
1 Anger
1 Animate Dead
1 Arcane Signet
1 Arid Mesa
1 Blackcleave Cliffs
1 Azra Oddsmaker
1 Badlands
1 Bazaar of Baghdad
1 Big Game Hunter
1 Blood Artist
1 Blood Crypt
1 Bloodstained Mire
1 Buried Alive
1 Butcher Ghoul
1 Cabal Ritual
1 Chainer, Nightmare Adept
1 Chaos Warp
1 Chrome Mox
1 City of Brass
1 Coalition Relic
1 Command Tower
1 Dance of the Dead
1 Dark Ritual
1 Demonic Tutor
1 Diabolic Intent
1 Dismember
1 Dockside Extortionist
1 Dragonskull Summit
1 Entomb
1 Exhume
1 Faithless Looting
1 Fellwar Stone
1 Fulminator Mage
1 Gamble
1 Graven Cairns
1 Cut // Ribbons
1 Imp's Mischief
1 Imperial Recruiter
1 Imperial Seal
1 Ingot Chewer
1 Kiki-Jiki, Mirror Breaker
1 Lightning Greaves
1 Living Death
1 Lotus Petal
1 Magus of the Moon
1 Magus of the Wheel
1 Underworld Breach
1 Mana Confluence
1 Mana Crypt
1 Mana Vault
1 Marsh Flats
1 Mausoleum Secrets
1 Mikaeus, the Unhallowed
1 Mogg Fanatic
6 Mountain
1 Murderous Redcap
1 Necromancy
1 Necrotic Ooze
1 Night's Whisper
1 Phyrexian Delver
1 Pyroblast
1 Reanimate
1 Rix Maadi Reveler
1 Scalding Tarn
1 Sidisi, Undead Vizier
1 Sire of Insanity
1 Smoldering Marsh
1 Sol Ring
1 Stitcher's Supplier
1 Sulfurous Springs
1 Sunscorched Desert
1 Thrill of Possibility
6 Swamp
1 Talisman of Indulgence
1 Thoughtseize
1 Torch Courier
1 Toxic Deluge
1 Triskelion
1 Urborg, Tomb of Yawgmoth
1 Vampiric Tutor
1 Verdant Catacombs
1 Victimize
1 Volrath's Stronghold
1 Wheel of Fortune
1 Worldgorger Dragon
1 Yawgmoth's Will
1 Zealous Conscripts