1 Bruse Tarl, Boorish Herder
1 Tymna the Weaver

1 Sol Ring
1 Mana Crypt
1 Sword of Feast and Famine
1 Sword of Fire and Ice
1 Umezawa's Jitte
1 Boros Signet
1 Orzhov Signet
1 Rakdos Signet
1 Thought Vessel
1 Chrome Mox
1 Mox Diamond
1 Smuggler's Copter
1 Elbrus, the Binding Blade
1 Talisman of Indulgence
1 Empyrial Plate
1 Mox Opal
1 Nihil Spellbomb
1 Swiftfoot Boots
1 Grafted Exoskeleton
1 Mask of Memory
1 Selfless Spirit
1 Hushwing Gryff
1 Aven Mindcensor
1 Stoneforge Mystic
1 Sire of Insanity
1 Linvala, Keeper of Silence
1 Sun Titan
1 Elesh Norn, Grand Cenobite
1 Dark Confidant
1 Queen Marchesa
1 Goblin Rabblemaster
1 Relic Seeker
1 Serra Ascendant
1 Weathered Wayfarer
1 Archangel of Thune
1 Ankle Shanker
1 Thalia, Heretic Cathar
1 Phyrexian Revoker
1 Recruiter of the Guard
1 Karlov of the Ghost Council
1 Leonin Relic-Warder
1 Drana, Liberator of Malakir
1 Leaden Myr
1 Iron Myr
1 Gold Myr
1 Knight of the White Orchid
1 High Priest of Penance
1 Godo, Bandit Warlord
1 Unspeakable Symbol
1 Dark Tutelage
1 Grasp of Fate
1 Anguished Unmaking
1 Swords to Plowshares
1 Path to Exile
1 Vampiric Tutor
1 Wear // Tear
1 Enlightened Tutor
1 Terminate
1 Rakdos Charm
1 Bloodstained Mire
1 Windswept Heath
1 Wooded Foothills
1 Polluted Delta
1 Flooded Strand
1 Blood Crypt
1 Godless Shrine
1 Sacred Foundry
1 Nomad Outpost
1 Command Tower
1 City of Brass
1 Mana Confluence
1 Swamp
1 Plains
1 Caves of Koilos
1 Sulfurous Springs
1 Battlefield Forge
1 Strip Mine
1 Bojuka Bog
1 Hall of the Bandit Lord
1 Gemstone Caverns
1 Ancient Tomb
1 Arid Mesa
1 Badlands
1 Clifftop Retreat
1 Dragonskull Summit
1 Plateau
1 Scrubland
1 Wasteland
1 Scalding Tarn
1 Verdant Catacombs
1 Shambling Vent
1 Lavaclaw Reaches
1 Shizo, Death's Storehouse
1 Ghost Quarter
1 Toxic Deluge
1 Demonic Tutor
1 Vandalblast
1 Burning Wish
