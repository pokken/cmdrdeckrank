1 Golos, Tireless Pilgrim

1 Academy Ruins
1 Ancient Tomb
1 Buried Ruin
1 Cascading Cataracts
1 Darksteel Citadel
1 Fiery Islet
1 Flooded Strand
1 Halimar Depths
1 High Market
1 Inventors' Fair
1 Mishra's Workshop
1 Misty Rainforest
1 Polluted Delta
1 Prismatic Vista
1 Scalding Tarn
1 Seat of the Synod
1 Shrine of the Forsaken Gods
18 Snow-Covered Island
1 Terrain Generator
1 Thawing Glaciers
1 Waterlogged Grove
1 All Is Dust
1 Arcane Denial
1 Basalt Monolith
1 Chromatic Lantern
1 Clock of Omens
1 Cloud Key
1 Coalition Relic
1 Consecrated Sphinx
1 Copy Artifact
1 Crucible of Worlds
1 Cryptic Command
1 Cyclonic Rift
1 Darksteel Forge
1 Fact or Fiction
1 Fellwar Stone
1 Gilded Lotus
1 Grim Monolith
1 Hangarback Walker
1 Krark-Clan Ironworks
1 Kuldotha Forgemaster
1 Mana Crypt
1 Mana Drain
1 Mana Vault
1 Manifold Key
1 Master Transmuter
1 Memory Jar
1 Memory Lapse
1 Metalworker
1 Mimic Vat
1 Mox Diamond
1 Mox Opal
1 Mystic Forge
1 Nevinyrral's Disk
1 Oblivion Stone
1 Phyrexian Metamorph
1 Power Artifact
1 Rings of Brighthearth
1 Rite of Replication
1 Scrap Trawler
1 Sculpting Steel
1 Sensei's Divining Top
1 Shimmer Myr
1 Sol Ring
1 Solemn Simulacrum
1 Spine of Ish Sah
1 Steel Hellkite
1 Tezzeret the Seeker
1 Tezzeret, Artifice Master
1 Thirst for Knowledge
1 Thran Dynamo
1 Trading Post
1 Transmute Artifact
1 Trinket Mage
1 Ugin, the Ineffable
1 Ugin, the Spirit Dragon
1 Unwinding Clock
1 Vedalken Orrery
1 Vedalken Shackles
1 Walking Ballista
1 Whir of Invention
1 Wurmcoil Engine