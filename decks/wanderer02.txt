1 Maelstrom Wanderer

1 Pathbreaker Ibex
1 Gilded Lotus
1 Skyshroud Claim
1 Thran Dynamo
1 Cultivate
1 Acidic Slime
1 Eternal Witness
1 Kodama's Reach
1 Oracle of Mul Daya
1 Consecrated Sphinx
1 Sakura-Tribe Elder
1 Explosive Vegetation
1 Brutalizer Exarch
1 Wood Elves
1 Somberwald Sage
1 Trygon Predator
1 Reclamation Sage
1 Trinket Mage
1 Sol Ring
1 Time Warp
1 Temporal Mastery
1 Jokulhaups
1 Mystical Tutor
1 Worldly Tutor
1 Urban Evolution
1 Bribery
1 Seasons Past
1 Recurring Insight
1 Cyclonic Rift
1 Beast Within
1 Fact or Fiction
1 Chaos Warp
1 Evacuation
1 Chromatic Lantern
1 Gruul Signet
1 Izzet Signet
1 Simic Signet
1 Thought Vessel
1 Mana Vault
1 Sylvan Library
1 Food Chain
1 Deadeye Navigator
1 Scourge of the Throne
1 Duplicant
1 Gilded Drake
1 Command Tower
1 Reflecting Pool
1 Breeding Pool
1 Steam Vents
1 Stomping Ground
1 Reliquary Tower
1 Cascade Bluffs
1 Fire-Lit Thicket
1 Flooded Grove
1 Frontier Bivouac
1 Kessig Wolf Run
1 Mountain
5 Island
5 Forest
1 Worn Powerstone
1 High Market
1 Grim Monolith
1 Hellkite Tyrant
1 Terastodon
1 Fierce Empath
1 Sun Quan, Lord of Wu
1 Taiga
1 Volcanic Island
1 Tropical Island
1 Misty Rainforest
1 Scalding Tarn
1 Wooded Foothills
1 Arid Mesa
1 Bloodstained Mire
1 Flooded Strand
1 Polluted Delta
1 Verdant Catacombs
1 Windswept Heath
1 Mana Crypt
1 Basalt Monolith
1 Temporal Manipulation
1 Capture of Jingzhou
1 Nature's Lore
1 Three Visits
1 Timetwister
1 Time Spiral
1 Fabricate
1 Crystal Shard
1 Rings of Brighthearth
1 Sensei's Divining Top
1 Tezzeret the Seeker
