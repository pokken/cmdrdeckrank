Commander 
1 Niv-Mizzet Reborn

Mono-colored combo (6)
1 Astral Slide
1 Astral Drift
1 Living End
1 Living Death
1 Hypergenesis
1 Eureka

Mono-colored other (4)
1 Eldrazi Displacer
1 Fires of Invention
1 Wilderness Reclamation
1 Yahenni's Expertise

Azorius (6)
1 Ardent Plea
1 Soulherder
1 Vanish into Memory
1 Brago, King Eternal
1 Sanctum Plowbeast
1 Medomai the Ageless

Dimir (6)
1 Ashiok, Dream Render
1 Perplex
1 Ancient Excavation
1 Jhessian Zombies
1 Dragonlord Silumgar
1 Lochmere Serpent

Rakdos (5)
1 Demonic Dread
1 Chainer, Nightmare Adept
1 Garna, the Bloodflame
1 Igneous Pouncer
1 Grave Upheaval

Gruul (6)
1 Violent Outburst
1 Klothys, God of Destiny
1 Rhythm of the Wild
1 Samut, Voice of Dissent
1 Valley Rannet
1 Gruul Ragebeast

Selesnya (5)
1 Faeburrow Elder
1 Sylvan Reclamation
1 Pale Recluse
1 Dragonlord Dromoka
1 Archon of Valor's Reach

Orzhov (5)
1 Kaya's Guile
1 Kaya, Ghost Assassin
1 Ashen Rider
1 Pyrrhic Revival
1 Primevals' Glorious Rebirth

Izzet (6)
1 Saheeli Rai
1 Expansion // Explosion
1 Ionize
1 Niv-Mizzet, Parun
1 Niv-Mizzet, the Firemind
1 The Locust God

Golgari (3)
1 Underrealm Lich
1 Cadaverous Bloom
1 Find // Finality

Boros (5)
1 Wear // Tear
1 Flame-Kin Zealot
1 Gerrard, Weatherlight Hero
1 Aurelia, the Warleader
1 Gisela, Blade of Goldnight

Simic (7)
1 Shardless Agent
1 Voidslime
1 Repudiate // Replicate
1 Tamiyo, Collector of Tales
1 Bring to Light
1 Prime Speaker Zegana
1 Tishana, Voice of Thunder

Lands (35)
1 Command Tower
1 City of Brass
1 Mana Confluence
1 Exotic Orchard
1 Canyon Slough
1 Fetid Pools
1 Irrigated Farmland
1 Scattered Groves
1 Sheltered Thicket
1 Plains
1 Island
1 Swamp
1 Mountain
1 Forest
1 Murmuring Bosk
1 Temple Garden
1 Stomping Ground
1 Steam Vents
1 Godless Shrine
1 Watery Grave
1 Hallowed Fountain
1 Breeding Pool
1 Overgrown Tomb
1 Sacred Foundry
1 Blood Crypt
1 Adarkar Wastes
1 Battlefield Forge
1 Brushland
1 Caves of Koilos
1 Karplusan Forest
1 Llanowar Wastes
1 Shivan Reef
1 Sulfurous Springs
1 Underground River
1 Yavimaya Coast

